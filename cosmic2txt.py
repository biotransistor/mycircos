##
# file: cosmic2hg18.py
# 
# history: 
#     author: bue
#     date: 2013-11-25
#     license: GPL >=3
#     language: Python 3.3.2
#     description: addapt Sanger CosmicCellLineProject_v67_241013.tsv (hg19) by transcript postion to genome bult version hg18. 
#     utilizing; UCSC genome table browser (assembly: Feb. 2009 (GRCh37/hg19) and Mar. 2006 (NCBI36/hg18); group: Genes and Gene Prediction Tracks; track: RefSeq Genes)
###

# rawcosmic2txtmaftxtgene

import sys 
import re 
import pyglue.glue as gl

def cdsmutsyntax2parser(mutation):
    #print(mutation)
    ref_allele = None
    tumor_seq_allele = None
    try:
        # point mutation
        regex_string = '[ACGT]+>[ACGT]+'
        parser = re.compile(regex_string)
        match = parser.search(mutation)
        ref_allele, tumor_seq_allele = match.group().split('>')
    except AttributeError as e:
        # delition and insertion
        tumor_seq_allele = '-'
        ref_allele = '-'
        try:
            regex_string = '[ACGT]+'
            parser = re.compile(regex_string)
            match = parser.search(mutation)
            # delition
            if mutation.find('del'):
                ref_allele = match.group()
            # insertin
            elif mutation.find('ins'):
                tumor_seq_allele = match
            # neider point mutation nor delition nor insertion 
            else: 
                sys.exit('Error: %s neider point mutation nor delition nor insertion'% mutation)
        except (AttributeError,TypeError) as e:
            pass        
            print('mutation without and amino bases specified')  # this case exist. e.g. del21
    # output
    return (ref_allele, tumor_seq_allele)
    

def stringkick(s_in):
   s_out = None
   s_in = s_in.strip(' ')
   s_in = s_in.strip('[')
   s_out = s_in.strip('] ')
   return(s_out)


if __name__=='__main__':
    # set uputput variable 
    # dlt-out = {}
    s_outputfile= 'cosmicv70breast.somatic.maf'
    lt_maf = []
    t_maflabel = ('Hugo_Symbol','Center','NCBI_Build','Chromosome','Start_Position','End_Position','Strand','Reference_Allele','Tumor_Seq_Allele1','Tumor_Seq_Allele2','Tumor_Sample_Barcode', )
    s_center = 'http://cancer.sanger.ac.uk/cancergenome/projects/cosmic/'
    s_build = 'hg19'
    # read out raw file 
    #s_infile = 'CosmicCellLineProject_v70_20140814.tsv'
    s_infile = 'CosmicCellLineProject_v70_20140814_breast.tsv'
    ts_key = ('Sample name','GRCh37 genome position',)
    ts_value = ('CDS_MUT_SYNTAX','GRCh37 strand','Gene name',)
    (dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_infile, tivtsvivs_value=('CDS_MUT_SYNTAX','GRCh37 strand','Gene name',), tivtsvivs_key=ts_key, tivtsvivs_primarykey=ts_key, ivs_labelrow=0, b_dtl=True)
    s_out = gl.pydt2txtrdb(s_outputfile='cosmiccellLineproject_process', dt_matrix=dt_matrix, t_xaxis=('primarykey','Tumor_Sample_Barcode','chr_start_end','cds_mut_syntax','strand','Hugo_Symbol'), s_mode='w')
    (dd_matrix, dd_yaxis, dd_xaxis) = gl.txtspre2pydd(s_matrix=s_out, tivs_yxaxis=(0,0))

    # grep information  
    # bue 20140905 : rwar file is aware of cancer type. use this and hg19 for file names of the igv track files!  
    for s_cellinelocus in dd_matrix.keys():
        # tumorsamplebarcode
        s_cellline = dd_matrix[s_cellinelocus]['Tumor_Sample_Barcode']
        s_cellline = stringkick(s_cellline)
        s_cellline = re.sub(r'\W+', '', s_cellline)
        s_cellline = s_cellline.upper()

        # locus
        s_locus = dd_matrix[s_cellinelocus]['chr_start_end']
        s_locus =  stringkick(s_locus)
        ts_fractal = s_locus.partition(':')
        s_chr = ts_fractal[0]
        s_startend = ts_fractal[2]
        ts_fractal = s_startend.partition('-')
        s_start = ts_fractal[0]
        s_end = ts_fractal[2]

        # strand
        s_strand = dd_matrix[s_cellinelocus]['strand']
        s_strand =  stringkick(s_strand)

        # cdsmutsyntax
        s_cdsmutsyntax = dd_matrix[s_cellinelocus]['cds_mut_syntax']
        (s_refallele, s_tumorseqallele) = cdsmutsyntax2parser(s_cdsmutsyntax)

        # hgnc
        s_hgnc = dd_matrix[s_cellinelocus]['Hugo_Symbol']
        s_hgnc = stringkick(s_hgnc)
        ts_fractal = s_hgnc.partition('_')
        s_hgnc = ts_fractal[0]

        # put into maf
        ts_maf = (s_hgnc, s_center, s_build, s_chr, s_start, s_end, s_strand, s_refallele, s_tumorseqallele,s_tumorseqallele, s_cellline)
        lt_maf.append(ts_maf)

        #print("BUE :", s_cellinelocus, s_tumorsamplebarcode, s_locus, s_chr, s_start, s_end, s_strand, s_cdsmutsyntax, s_refallele, s_tumorseqallele, s_hgnc)
        # put into dlt 
        # get lt 
        #try : 
        #    lt_out = dlt-out[s_tumorsamplebarcode]
        #except :
        #    lt_out = [] 
        # put into lt         
        #lt_out.add

    # write maf file 
    # print('lt_maf :',lt_maf)
    s_out = gl.pylt2txtrdb(s_outputfile=s_outputfile, lt_matrix=lt_maf, t_xaxis=t_maflabel, s_mode='w') 
    #return(s_out)
